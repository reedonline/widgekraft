var server = require('./app/server').createServer(),
    port = server.get('port') || 9000;

server.listen(port, function(){
  console.log('Server listening on port', port);
});