// modules
var express = require('express'),
    path = require('path');

// middleware
var bodyParser = require('body-parser'),
    favicon = require('serve-favicon'),
    serveStatic = require('serve-static'),
    morgan = require('morgan'),
    errorHandler = require('errorhandler'),
    methodOverride = require('method-override'),
    routes = require('./routes');

// export server
exports.createServer = function createServer() {
  // setup
  var server = express(),
      node_env = server.get('env') || 'development';

  // configure server
  server.set('port', process.env.PORT || 9000);
  server.engine('jade', require('jade').__express);
  server.set('views', path.join(__dirname, 'views'));
  server.set('view engine', 'jade');

  // middleware
  //server.use(favicon(__dirname + '/public/favicon.ico'));
  server.use(morgan());
  server.use(bodyParser());
  server.use(methodOverride());
  server.use(serveStatic(path.join(__dirname, 'public')));

  // attach routes
  require('./routes')(server);

  // 404
  server.use(function pageNotFound(req, res, next) {
    res.status(404);

    if (req.accepts('html')) {
      return res.render('shared/404', { url: req.url });
    }

    if (req.accepts('json')) {
      return res.send({ error: 'Page not found'});
    }

    return res.type('txt').send('Page not found');
  });

  // 500
  server.use(function serverError(error, req, res, next) {
    res.status(500);

    if (req.accepts('html')) {
      return res.render('shared/500', { url: req.url, error: error });
    }

    if (req.accepts('json')) {
      return res.send({ error: error });
    }

    return res.type('txt').send('Internal server error');
  });

  // development only
  if ('development' == node_env) {
    server.use(errorHandler());
  }

  return server;
};