var widgekraft = widgekraft || (function widgekraft(){

  var w = {
    arc: {
      init: function(options){
        options = options || {};

        var percentage = Math.round(+options.percentage || 77) / 100;
        var width = options.width || 500,
            height = options.height || 500,
            τ = 2 * Math.PI; // http://tauday.com/tau-manifesto

        // An arc function with all values bound except the endAngle. So, to compute an
        // SVG path string for a given angle, we pass an object with an endAngle
        // property to the `arc` function, and it will return the corresponding string.
        var arc = d3.svg.arc()
            .innerRadius(180)
            .outerRadius(240)
            .startAngle(0);

        d3.select("#visualisation").select("svg").remove();
        // Create the SVG container, and apply a transform such that the origin is the
        // center of the canvas. This way, we don't need to position arcs individually.
        var svg = d3.select("#visualisation").append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
              .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        // Add the background arc, from 0 to 100% (τ).
        var background = svg.append("path")
            .datum({endAngle: τ})
            .style("fill", options.background || "#eee")
            .attr("d", arc);

        // Add the foreground arc in orange, currently showing 12.7%.
        var foreground = svg.append("path")
            .datum({endAngle: percentage * τ})
            .style("fill", options.foreground || "orange")
            .attr("d", arc);

        // Add some text
        var text = svg.append("text")
            .datum({ percentage: percentage * 100 })
            //.enter()
            .text("0%")
            .style("fill", options.foreground || "orange")
            .attr({
              "font-family": "sans-serif",
              "font-size": "100px",
              "text-anchor": "middle",
              "transform": "translate(0,35)"
            });

        foreground.transition()
          .duration(750)
          .call(arcTween);

        text.transition()
          .duration(750)
          .tween("text", function (d) {
            console.log(d);
            var i = d3.interpolate(parseInt(this.textContent), d.percentage);

            return function(t) {
              this.textContent = Math.round(i(t)) + "%";
            };
          });

        function arcTween(transition) {
          transition.attrTween("d", function(d) {
            var interpolate = d3.interpolate(0, d.endAngle);
            return function(t) {
              d.endAngle = interpolate(t);
              return arc(d);
            };
          });
        }

        var href = w.toBase64();
        var $download = $('#download-visualisation');
        if (href) {
          $download.attr('href', href).show();
        } else {
          $download.attr('href', '#').hide();
        }
      }
    },
    toBase64: function(){
      window.URL = window.URL || window.webkitURL;

      try{
        // assumes only one svg in the page
        var svg = document.querySelector('#visualisation svg');
        var source = (new XMLSerializer()).serializeToString(svg);
        var base64 = btoa(source);
        var dataURI = 'data:image/svg+xml;base64,' + base64;
      } catch (e) {}

      return dataURI || '';
    }
  };

  return w;

})();