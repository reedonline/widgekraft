var express = require('express'),
    path = require('path'),
    app = express();

// controllers
var home = require('../controllers/home'),
    chart = require('../controllers/chart');

module.exports = function attachRoutes(parent) {
  var env = app.get('env') || 'development';

  if (env !== 'production') {
    console.log('Attaching routes');
  }

  // reset views (as we're a directory down)
  app.set('views', path.join(__dirname, '../views'));


  /***********************************************
   * Home
   ***********************************************/

  // GET: /
  app.get('/', home.index);


  /***********************************************
   * Chart
   ***********************************************/

  // GET: /
  app.get('/chart', chart.index);

  // GET: /
  app.get('/chart/arc', chart.arc);

  parent.use(app);
};